from django.contrib import admin
from blog.models import Post, Comment, Tags, Notifilcation, PostStats, UserViews, SubComment

# Register your models here.

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Tags)
admin.site.register(Notifilcation)
admin.site.register(PostStats)
admin.site.register(UserViews)
admin.site.register(SubComment)