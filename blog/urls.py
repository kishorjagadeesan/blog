# urls.py

from django.conf.urls import url, include
from blog import views

app_name = 'blog'

urlpatterns = [
	url(r'home', views.HomeView.as_view(), name='home'),
	url(r'about', views.AboutView.as_view(), name='about'),
	url(r'contactus', views.ContatView.as_view(), name='contactus'),
	url(r'notificatios', views.GetNotifications.as_view(), name='notofications'),
	url(r'get_fields', views.GetColumnData.as_view(), name='get_fields'),

	url(r'^post/post_list', views.GetPostList.as_view()),
	url(r'^post/(?P<slug>[-\w]+)$', views.DetailPostView.as_view(), name='post_details'),
	url(r'post/comment/add_comment$', views.AddComment.as_view(), name='add_comment'),
	url(r'post/comment/update_comment$', views.CommentActions.as_view(), name='update_comment'),
	url(r'^post/edit/(?P<pk>\d+)$', views.EditBlogView.as_view(), name='edit_blog'),
	url(r'^post/update/(?P<pk>\d+)$', views.UpdateBlogView.as_view(), name='update_blog'),
	url(r'post/comment/add_update_subcomment', views.SubCommentActions.as_view(), name='sub_comment'),

	url(r'create', views.CreateBlogView.as_view(), name='create'),
	url(r'drafts$', views.DraftView.as_view(), name='drafts'),
	url(r'delete_post/(?P<pk>\d+)$', views.DeletePostView.as_view(), name='delete_post'),
	url(r'draft_details', views.GetDraftDetails.as_view(), name='dtaft_details'),
	url(r'update_draft', views.UpdateDraft.as_view(), name='update_draft'),
	url(r'my_blogs', views.MyTechBlogs.as_view(), name='my_blogs'),
	url(r'^get_tags/', views.GetBlogTags.as_view(), name='get_tags'),
	

]