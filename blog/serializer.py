#serializer.py

from rest_framework import serializers
from blog.models import Comment, Post
from user_auth.models import UserDetails
from django.contrib.auth.models import User

class UserDetailsSerializer(serializers.ModelSerializer):
	class Meta():
		model = UserDetails
		fields = ('middle_name', 'slug')

class UserSerializer(serializers.ModelSerializer):
	user_details = UserDetailsSerializer(many=False, read_only=True)
	class Meta():
		model = User
		fields = ('pk', 'first_name', 'last_name', 'user_details')

class CommentSerializer(serializers.ModelSerializer):
	author = UserSerializer(many=False, read_only=True)
	class Meta():
		model = Comment
		fields = ('pk', 'text', 'approved_comments', 'author', 'post')

class PostSerializer(serializers.ModelSerializer):
	class Meta():
		model=Post
		fields=('title', 'slug')