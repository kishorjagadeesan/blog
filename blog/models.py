from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.db.models.signals import post_save, post_delete, pre_save
from slugify import slugify

# Create your models here.

MESSAGE_TYPE = (
	('comment', 'comment'),
	('post', 'post'),
	('profile', 'profile')
)

class Post(models.Model):
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	title = models.CharField(max_length=200, unique=True)
	text = models.TextField()
	created_date = models.DateTimeField( auto_now_add=True)
	published_date = models.DateTimeField(blank=True, null=True)
	tags = models.CharField(max_length=200, blank=True, null=True)
	updated_date = models.DateTimeField(auto_now=True)
	slug= models.SlugField(max_length=200, blank=True, null=True, unique=True)
	
	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('blog:drafts')

class Comment(models.Model):
	post = models.ForeignKey('blog.Post', related_name='comments', on_delete=models.CASCADE)
	author = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True, blank=True)
	text = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
	approved_comments = models.BooleanField(default=False)

	def approve(self):
		self.approved_comments = True
		self.save()

	def __str__(self):
		return self.text

class Tags(models.Model):
	tags = models.CharField(max_length=300)
	post = models.ForeignKey('blog.Post', related_name='tag_list', on_delete=models.CASCADE)

	def get_tag_list(self):
		return self.tags.split(',')

class Notifilcation(models.Model):
	reciever = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True, related_name='revieved_notifications')
	sender = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True, related_name='send_notifications')
	message = models.CharField(max_length=300, null=False, blank=False)
	comment = models.ForeignKey('blog.Comment', on_delete=models.CASCADE, null=True, related_name='comment_notifications')
	created_date = models.DateTimeField(auto_now_add=True)
	viewed_date = models.DateTimeField(null=True, blank=True)
	active = models.BooleanField(null=False, blank=False, default=True)
	message_type=models.CharField(max_length=10, null=True, blank=True, choices=MESSAGE_TYPE)

	def __str__(self):
		return self.message

class PostStats(models.Model):
	post = models.OneToOneField('blog.Post', related_name='stats', on_delete=models.CASCADE)
	last_view = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	number_of_views = models.IntegerField(default=0)
	unique_viwes = models.IntegerField(default=0)

	def __str__(self):
		return self.post.title + ' (' + str(self.number_of_views) + ')'

	def add_num_view(self):
		self.number_of_views +=1

	def set_last_view(self):
		self.last_view = timezone.now()

	def set_unique_views(self):
		self.unique_viwes +=1


class UserViews(models.Model):
	user = models.ForeignKey('auth.User', related_name='user_views', on_delete=models.CASCADE, null=True)
	post = models.ForeignKey('blog.Post', related_name='blog_views', on_delete=models.CASCADE, null=True)
	view_date = models.DateTimeField(auto_now_add=True)

	def update_viewed_date(self):
		self.view_date = timezone.now()


class SubComment(models.Model):
	comment = models.ForeignKey('blog.Comment', on_delete=models.CASCADE, related_name='sub_comments')
	author = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True)
	comment_text = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
	updated_date = models.DateTimeField(auto_now=True)
	approved_comments = models.BooleanField(default=False)


'''
def comment_post_delete_receiver(sender, instance, created, *args, **kwargs):
	print("inside the signal function:", created)
	if instance:
		notifications = Notifilcation.objects.filter(comment=instance)
		if notifications:
			for notifilcation in notifications:
				notifilcation.delete()


post_delete.connect(comment_post_delete_receiver, sender=Comment)
'''

def blog_pre_save_receiver(sender, instance, raw, *args, **kwargs):
	if instance:
		instance.slug = slugify(instance.title)

pre_save.connect(blog_pre_save_receiver, sender=Post)