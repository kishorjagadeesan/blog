from django.shortcuts import render, render_to_response
from django.utils import timezone
from django.views.generic import (View, TemplateView, ListView, DetailView
									,CreateView, UpdateView, DeleteView)

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse, reverse_lazy
from django.views.generic.edit import ModelFormMixin
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.template import RequestContext
from django.db.models import Q
from django.contrib.auth.models import User

from blog.models import Post, Comment, Tags, PostStats, Notifilcation, UserViews, SubComment
from blog.forms import PostForm
from blog.serializer import CommentSerializer, PostSerializer

from slugify import slugify
import json
from blog.model_utils import ModelUtils

class HomeView(ListView):
	models = Post
	paginate_by = 3
	context_object_name = 'post_list'
	template_name = 'blog/home.html'

	def get_queryset(self):
		
		search = self.request.GET.get('search', '')
		if search:
			return Post.objects.filter(	Q(title__icontains=search) 
										| Q(text__icontains=search)
										| Q(tags__icontains=search), published_date__lte=timezone.now()).order_by('-published_date')
		else:
			return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')



	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		search = self.request.GET.get('search', '')
		context['search'] = search
		context['page_name'] = 'blog_home'
		if self.request.user.is_authenticated:
			context['last_viewed'] = UserViews.objects.filter(user=self.request.user).order_by('-view_date')[:5]
		return context;

class DraftView(LoginRequiredMixin, ListView):
	model = Post
	template_name = 'blog/draft.html'

	def get_queryset(self):
		return Post.objects.filter(published_date__isnull=True, author=self.request.user).order_by('-created_date')

class DetailPostView(LoginRequiredMixin, DetailView):
	model = Post
	template_name="blog/view_post.html"
	slug_url_kwarg = 'slug'
	
	def get_context_data(self, **kwargs):
		context = super(DetailView, self).get_context_data(**kwargs)
		ModelUtils.update_blog_stats(context['object'], self.request.user)
		context['page_name'] = 'view_post'
		return context

class CreateBlogView(CreateView):
	template_name = 'blog/create_blog.html'
	form_class = PostForm

	def form_valid(self, form):
		self.obj = form.save(commit=False)
		self.obj.author = self.request.user
		self.obj.slug = slugify(self.obj.title)
		self.obj.save()
		return super(CreateBlogView, self).form_valid(form)


class DeletePostView(LoginRequiredMixin, View):
	
	def get(self,request, *args, **kwargs):
		post_id = kwargs.get('pk', '')
		context = {};
		if post_id:
			try:
				post = Post.objects.get(pk=post_id, author=request.user)
			except Post.DoesNotExist:
				post = ''
			if post:
				post.delete()
				context['success'] = 'Post deleted Successfully!!'
			else:
				context['error'] = 'Oops!!. Some error occured. :('
		else:
			context['error'] = 'Oops!!. Some error occured. :('
		return JsonResponse(context)

class GetDraftDetails(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		draft_id = request.GET.get('draft_id', '')
		if draft_id:
			try:
				post = Post.objects.get(pk=draft_id, author=request.user)
			except Post.DoesNotExist:
				post = ''
			if post:
				return JsonResponse(serializers.serialize('json', [post], 
						fields=('title', 'text', 'author', 'created_date', 'tags')), safe=False)
			else:
				return JsonResponse({'error': 'Oops!!. Some error occured. :('})
		return JsonResponse({'error': 'Oops!!. Some error occured. :('})


class UpdateDraft(LoginRequiredMixin, UpdateView ):
	form_class = PostForm
	template_name = 'blog/draft.html'
	
	def get_object(self):
		return Post.objects.get(pk=self.request.POST.get('post_id'), author=self.request.user)

	def form_valid(self, form):
		self.obj=form.save()
		if self.request.POST.get('submit_type') == 'Post':
			self.obj.published_date = timezone.now()
			self.obj.save()
			messages.add_message(self.request, messages.SUCCESS, 'Draft Published Successfully..!!')
		else:
			messages.add_message(self.request, messages.SUCCESS, 'Draft Updated Successfully..!!')

		return super(UpdateDraft, self).form_valid(form)

	def form_invalid(self, form):
		messages.add_message(self.request, messages.SUCCESS, 'Oops Some Error Occured.. :)')
		return HttpResponseRedirect(reverse('blog:drafts'))

	def get_success_url(self):
		return reverse('blog:drafts')

class AddComment(LoginRequiredMixin, View):

	def post(self,request, *args, **kwargs):
		# print("\najax check: ", request.is_ajax())
		context = {};
		if request.POST.get('post_id') and request.POST.get('comment'):
			try:
				post = Post.objects.get(pk=request.POST.get('post_id'))
			except Post.DoesNotExist:
				post = ''
			if post:
				comment = Comment(post = post, author=request.user, text=request.POST.get('comment'))
				#if request.user == post.author:
				comment.approved_comments = True
				comment.save()

				if request.user != post.author:
					ModelUtils.add_notifications(request.user, post.author, 'new_comment', 
											comment=comment, post_title=post.title,)
				if request.POST.get('as_json'):
					serializer = CommentSerializer(instance=comment)
					context['log_user'] = request.user.pk
					context['post_author'] = post.author.pk
					context['comment'] = serializer.data
					return JsonResponse(context, safe=False)
				else:
					return render(request, 'blog/widgets/new_comment.html', {'comment':comment})
		return JsonResponse({'messages': 'Oops Some Error Occured.. :)'})

class CommentActions(LoginRequiredMixin, View):
	def post(self, request, *args, **kwargs):
		context ={};
		comment_id = request.POST.get('comment_id')
		user_action = request.POST.get('user_action')
		if (comment_id and user_action):
			try:
				comment = Comment.objects.get(pk=comment_id)
			except Comment.DoesNotExist:
				comment = ''

			if comment:
				if user_action == 'delete':
					if ((comment.author == request.user) or (comment.post.author == request.user)):
						comment.delete()
						context['delete'] = 1
				elif user_action == 'approve':
					if comment.post.author == request.user:
						comment.approve()
						context['approve'] = 1
						ModelUtils.add_notifications(request.user, comment.author, 'comment_approve', 
								title=comment.post.title, comment=comment)
				elif user_action == 'edit':
					if comment.author == request.user:
						context['edit'] = 1
						if request.POST.get('comment_text_update'):
							comment.text = request.POST.get('comment_text_update')
							comment.save() 
				return JsonResponse(context);
			else:
				return JsonResponse({'messages': 'Oops..Some error occured.. :('})
		return JsonResponse({'messages': 'Oops..Some error occured.. :('})

class MyTechBlogs(LoginRequiredMixin, ListView):
	template_name = 'blog/my_blogs.html'
	model = Post

	def get_queryset(self):
		return Post.objects.filter(author=self.request.user, published_date__isnull=False).order_by('-published_date')

class EditBlogView(LoginRequiredMixin, DetailView):
	template_name = 'blog/edit_blogs.html'
	model = Post
	fields = ('title', 'text', 'tags')

	def get_success_url(self):
		return reverse('blog:my_blogs')

class UpdateBlogView(LoginRequiredMixin, UpdateView):
	#template_name =
	model = Post
	fields = ('title', 'text', 'tags')
	template_name = 'blog/edit_blogs.html'

	def get_success_url(self):
		return reverse('blog:my_blogs')


class GetBlogTags(View):
	def get(self, request, *args, **kwargs):
		post_id = request.GET.get('post_id')
		if post_id:
			try:
				post = Post.objects.get(pk=post_id).tags
				tag_list = [tag.strip() for tag in post.split(',')]
				return JsonResponse({'tags': list(set(tag_list))})
			except Post.DoesNotExist:
				post = ''
		else:	
			post = Post.objects.all().values_list('tags')
			tag_list = []
			for row in post:
				if row[0]:
					tags = [tag.strip() for tag in row[0].split(',')]
					for tag in tags:
						if tag not in tag_list:
							tag_list.append(tag)
			return JsonResponse({'tags': tag_list[:50]})
		return JsonResponse({'error': 1})

class GetPostList(View):
	def get(self, request, *args, **kwargs):
		search_str = request.GET.get('search_text', '')
		if search_str:
			query_set = Post.objects.filter(title__icontains=search_str, published_date__isnull=False)
			searialie_data = PostSerializer(query_set, many=True)
			return JsonResponse(searialie_data.data, safe=False)
		else:
			return JsonResponse({})

class SubCommentActions(View, LoginRequiredMixin):
	
	def post(self, request, *args, **kwargs):
		comment_id = request.POST.get('comment_id')
		text = request.POST.get('text')
		sub_comment_id = request.POST.get('sub_comment_id')

		context = {}
		if sub_comment_id:
			action = request.POST.get('action')
			try:
				sub_comment = SubComment.objects.get(pk=sub_comment_id)
			except SubComment.DoesNotExist:
				sub_comment = None
				context['error'] = 1
			if sub_comment:
				if action == 'delete' and (sub_comment.author == request.user or sub_comment.comment.author == request.user):
					sub_comment.delete()
					context['success'] = 1
				elif action == 'edit' and sub_comment.author == request.user and text:
					sub_comment.comment_text = text
					sub_comment.save()
					context['success'] = 1
			else:
				return JsonResponse(context)
		else:
			try:
				comment = Comment.objects.get(pk=comment_id)
			except Comment.DoesNotExist:
				comment= None
			if comment:
				sub_comment = SubComment.objects.create(comment=comment, author=request.user, comment_text=text)
				if comment.author != request.user:
					ModelUtils.add_notifications(request.user, comment.author, 'sub_comment', comment=comment)
				return render(request, 'blog/widgets/new_sub_comment.html', {'subcomment': sub_comment})

		return JsonResponse(context)

class GetNotifications(View, LoginRequiredMixin):
	def get(self, request, *args, **kwargs):
		notification_type = request.GET.get('notification_type')
		sender = request.GET.get('sender')
		reciever = request.GET.get('reciever')
		status = request.GET.get('status')
		send_or_receive = request.GET.get('send_or_receive', '')
		output = {'new': {}, 'read': {}}

		if send_or_receive == 'send':
			notifications = Notifilcation.objects.filter(sender=request.user).order_by('created_date')
		elif send_or_receive == 'receive':
			notifications = Notifilcation.objects.filter(reciever=request.user).order_by('created_date')
		else:
			notifications = Notifilcation.objects.filter(Q(sender=request.user) | Q(reciever=request.user)).order_by('created_date')

		if notifications:
			if notification_type != 'all':
				notifications = notifications.filter(message_type=notification_type)
			
			if status:
				if status == 'active':
					notifications = notifications.filter(active=True)
				elif status == 'inactive':
					notifications = notifications.filter(active=False)

			for notification in notifications:
				if notification.active:
					status = 'new'
				else:
					status = 'read'
				message_type = notification.message_type
				if message_type not in output[status]:
					output[status][message_type] = []

				data = {
					'sender' 		: notification.sender.first_name + ' ' + notification.sender.last_name,
					'sender_id'		: notification.sender.pk,
					'receive'		: notification.reciever.first_name + ' ' + notification.reciever.last_name,
					'receive_id'	: notification.reciever.pk,
					'message'		: notification.message,
					'message_type'	: notification.message_type,
					'created_date'	: notification.created_date,
					'viewed_date'	: notification.viewed_date,
				}
				output[status][message_type].append(data)
			return JsonResponse(output)
		else:
			return JsonResponse({})

class GetColumnData(LoginRequiredMixin, View):
	def post(self, request, *args, **kwargs):
		#fields = json.loads(request.POST.get('fields'))
		model = request.POST.get('model')
		data_id = request.POST.get('data_id')
		slug = request.POST.get('slug')
		context = {'fields': {}}
		data = None
		if model == 'Comment':
			try:
				data = Comment.objects.get(pk=data_id, author=request.user)
				context['fields']['pk'] = data.pk
				context['fields']['text'] = data.text
			except Comment.DoesNotExist:
				pass

		elif model == 'SubComment':
			try:
				data = SubComment.objects.get(pk=data_id, author=request.user)
				context['fields']['pk'] = data.pk
				context['fields']['text'] = data.comment_text
			except Comment.DoesNotExist:
				pass
		if data:
			context['success'] = 1
		return JsonResponse(context)
		
##########################################
# Static Templates
##########################################

class AboutView(TemplateView):
	template_name = 'blog/about.html'

class ContatView(TemplateView):
	template_name = 'blog/contact.html'


##########################################
# Error Pages
##########################################


def handler404(request, exception, template_name='404.html'):
	return render(request, template_name, {}, status=404)
	

def handler500(request, template_name='500.html'):
	return render(request, template_name, {}, status=500)