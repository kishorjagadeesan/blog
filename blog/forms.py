# forms.py

from django import forms
from blog.models import Post, Comment
from django.db.models import Q
from slugify import slugify

class PostForm(forms.ModelForm):

	post_id = forms.IntegerField(required=False)
	class Meta():
		model = Post
		fields = ('title', 'text', 'tags',)

	def clean(self):
		cleaned_data = super(PostForm, self).clean()

		if cleaned_data.get('title'):
			post_count = Post.objects.filter(
					Q(slug=slugify(cleaned_data.get('title')))
					| Q(title=cleaned_data.get('title'))).exclude(pk=cleaned_data.get('post_id')).count()
			if post_count:
				self.add_error('title', 'Post with similar title already exist. ' 
					+ 'Please confirm similar post not exist in the site.')

'''
		widgets = {
			'title' : forms.TextInput(attrs={'class':'textinputclass'}),
			'text'  : forms.TextInput(attrs={'class': 'editable medium-editor-textarea postcontent'})
		}

class CommentForm(forms.ModelForm):
	class Meta():
		model = Comment
		fields = ('text')

		widgets = {
			'text' : forms.TextInput(attrs={'class': 'editable medium-editor-textarea postcontent'})
		}
'''