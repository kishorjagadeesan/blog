# model_utils.py
from blog.models import PostStats, UserViews, Notifilcation, Post, Comment
from django.utils import timezone

MESSAGE_LIST = {
		'new_comment': "New comment is added to the post '{}'. Please do the necessary actions.", 
		'comment_approve': "Your comment added on post '{}' is approved by the post author.",
		'sub_comment': "'{}' replied to one of your comment on post '{}'. Please review.",
	}

class ModelUtils():

	def update_blog_stats(post, user=None):
		print(post, user)

		try:
			stat = PostStats.objects.get(post=post)
		except PostStats.DoesNotExist:
			stat = None

		try:
			user_view = UserViews.objects.get(post=post, user=user)
		except UserViews.DoesNotExist:
			user_view = None

		if stat:
			stat.set_last_view()
			if not user or (user and post.author.id == user.id and stat.number_of_views < 1) \
				or user.id != post.author.id:
				stat.add_num_view()
			if not user_view:
				stat.set_unique_views()
			stat.save()
		else:
			stat = PostStats(post=post)
			stat.add_num_view()
			if user:
				stat.set_unique_views()
			stat.save()

		if user_view:
			user_view.update_viewed_date()
			user_view.save()
		else:
			user_view = UserViews(post=post, user=user)
			user_view.save()

	def add_notifications(sender=None, receiver=None, message_type='', *args, **kwargs):

		if message_type == 'new_comment':
			Notifilcation.objects.create(
					sender=sender,
					reciever=receiver,
					message=MESSAGE_LIST[message_type].format(kwargs['post_title']),
					message_type='comment',
					comment=kwargs['comment']
				)

		elif message_type == 'comment_approve':
			Notifilcation.objects.create(
					sender=sender,
					reciever=receiver,
					message=MESSAGE_LIST[message_type].format(kwargs['title']),
					message_type='comment',
					comment=kwargs['comment']
				)
		elif message_type == 'sub_comment':
			Notifilcation.objects.create(
					sender=sender,
					reciever=receiver,
					message=MESSAGE_LIST[message_type].format(sender.first_name, kwargs['comment'].post.title),
					message_type='comment',
					comment=kwargs['comment']
				)