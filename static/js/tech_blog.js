
function display_popup_message(message, type){
	var div_class="alert-primary";
	
	if (type=='success'){
		div_class="alert-success";
	}else if (type=='error'){
		div_class="alert-danger";
	}

	$("#alert_div").removeClass('alert-primary');
	$("#alert_div").removeClass('alert-success');
	$("#alert_div").removeClass('alert-danger');
	$("#alert_div").addClass(div_class);
	$("#alert_div").html(message);
	$("#alert_div").fadeIn(1000).delay(1000).fadeOut(3000);
}

function display_errors(error_hash, parent_id){

	if (typeof(error_hash) != 'object'){
		error_hash = JSON.parse(error_hash);
	}

	if (parent_id){
		$('#'+parent_id +' .field-error').remove();
	}else{
		$('.field-error').remove();
	}

	var fields = Object.keys(error_hash);
	for (var i=0; i < fields.length; i++){
		var field_name = fields[i];
		var error_field = '<span class="field-error" id="span_'+ field_name +'">';
		var error_list = error_hash[field_name];
		for(var j=0; j<error_list.length; j++){
			error_field+= error_list[j]['message'] + '<br/>';
		}
		error_field+= '</span>';
		$('#'+field_name).after(error_field);

	}
}
