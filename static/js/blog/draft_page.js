//draft_page.js

$(document).ready(function(){
	$('.delete-img').click(function(){
		var draft_id = $(this).attr('id').replace('delete_', '');
		if (confirm('Are you sure you want to delete this draft?')){
			$.ajax({
				type: 'get',
				url: 'delete_post/'+draft_id, 
				success: function(response){
					console.log(response);
					if (response['error']){
						display_popup_message(response['error'], 'error');
					}else if (response['success']){
						display_popup_message(response['success'], 'error');
						$('#row_'+draft_id).remove();
					}else{
						display_popup_message('Oops Some error occured..!!', 'error');
					}
				},
				error: function(response){
					display_popup_message('Oops Some error occured..!!', 'error');
				}

			});
		}
	});

	$('.edit-img').click(function(){
		var draft_id = $(this).attr('id').replace('edit_', '');
		$('#post_id').val('');
		$.ajax({
			type: 'get',
			url: 'draft_details',
			data: {'draft_id': draft_id},
			success: function(response){
				var json_data = JSON.parse(response)[0]
				$('#title').val(json_data['fields']['title'])
				$('#text').val(json_data['fields']['text'])
				$('#tags').val(json_data['fields']['tags'])
				$('#post_id').val(json_data['pk']);
			},
			error: function(response){
				display_popup_message('Oops Some error occured..!!', 'error');
			}
		});
	});

	$('.model-btn').click(function(){
		$('#submit_type').val($(this).val());
	});
});