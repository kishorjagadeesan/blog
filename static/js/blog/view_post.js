//view_post.js

$(document).ready(function(){
	$('#add_comment').click(function(){
		$.ajax({
			type: 'post',
			url: "/blog/post/comment/add_comment",
			data: {
				post_id: $('#post_id').val(),
				comment: $('#comment_text').val(),
				csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()
			},
			success: function(response){
				$('#comment_div').append(response);
				$('#comment_text').val('');
				display_popup_message('Comment added successfully..! :)', 'success');
			},
			error: function(response){
				alert('Oops some error occured.. :(');
			}
		});
	});

	$('body').on('click', '.edit-img', function(){
		var id = $(this).attr('data-id');
		fields_req = ['pk', 'text'];
		$.ajax({
			type : 'post',
			url : '/blog/get_fields',
			data: {
				fields : JSON.stringify(fields_req),
				model: 'Comment',
				data_id: id,
				csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
			},
			success: function(response){
				if (response['success']){
					console.log(response);
					$('#comment_text_update').val(response['fields']['text']);
					$('#update_comment_id').val(response['fields']['pk']);
					$('#comment_model').modal('toggle');
				}else{
					display_popup_message('Oops Some error occured..!! :(', 'error');
				}
			},

			error: function(response){
				display_popup_message('Oops Some error occured..! :(', 'error');
			},		
		});
	});

	$('body').on('click', '#update_cmd_btn', function(){

		var text = $('#comment_text_update').val();
		var comment_id = $('#update_comment_id').val();
		if (text){
			$.ajax({
				type : 'post',
				url : '/blog/post/comment/update_comment',
				data: {
					comment_id : comment_id,
					user_action: 'edit',
					comment_text_update: text,
					csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
				},
				success: function(response){
					$('#comment_text_'+comment_id).html(text);
					$('#comment_model').modal('toggle');
					display_popup_message('Comment updated successfully..! :)', 'success');	
				},

				error: function(response){

				},		
			});
		}
	});

	$('body').on('click', '.delete-img', function(){
		var user_action = $(this).attr('data-uaction');
		var comment_id = $(this).attr('data-id');
		clicked_element = $(this);
		user_confirm = confirm("Are you sure you want to "+ user_action + " this comment?");
		if (comment_id && user_action && user_confirm){
			$.ajax({
				type : 'post',
				url : '/blog/post/comment/update_comment',
				data: {
					comment_id : comment_id,
					user_action: user_action,
					csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
				},

				success: function(response){
					if(response['delete']){
						$('#div_'+comment_id).remove();
						display_popup_message('Comment deleted successfully..! :)', 'success');
					}else if (response['approve']){
						display_popup_message('Comment approved successfully..! :)', 'success');
						$(clicked_element).remove();
					}
				},

				error: function(){
					display_popup_message('Oops Some error occured..! :(', 'error');
				},
			});
		}
	});

	$('body').on('click', '.sub-commend-btn', function(){
		var id = $(this).attr('id').replace('add_sub_comment_', '');
		text = $('#sub_comment_text_'+id).val();

		if (text){
			$.ajax({
				type : 'post',
				url : '/blog/post/comment/add_update_subcomment',
				data: {
					comment_id: id,
					text: text,
					csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
				},
				success: function(response){
					$('#sub_comment_'+id).append(response);
					$('#sub_comment_text_'+id).val('');
				},

				error: function(response){
					display_popup_message('Oops Some error occured..! :(', 'error');
				},
			});
		}
	});

	$('body').on('click', '.sub-delete-img', function(){
		var comment_id = $(this).attr('data-id');
		if (confirm("Are you sure you want to delete this comment?")){
			$.ajax({
				type : 'post',
				url : '/blog/post/comment/add_update_subcomment',
				data: {
					sub_comment_id : comment_id,
					action: 'delete',
					csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
				},

				success: function(response){
					if(response['success']){
						$('#sub_comment_div_'+comment_id).remove();
						display_popup_message('Comment deleted successfully..! :)', 'success');

					}
				},

				error: function(){
					display_popup_message('Oops Some error occured..! :(', 'error');
				},
			});
		}		
	});

	$('body').on('click', '.sub-edit-img', function(){
		var id = $(this).attr('data-id');
		var text = $('#sub_comment_text_'+id).html();
		$('#update_sub_comment').val(text);
		$('#update_sub_comment_id').val(id);
	});

	$('body').on('click', '.sub-edit-img', function(){
		var id = $(this).attr('data-id');
		fields_req = ['pk', 'text'];
		$.ajax({
			type : 'post',
			url : '/blog/get_fields',
			data: {
				//fields : JSON.stringify(fields_req),
				model: 'SubComment',
				data_id: id,
				csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
			},
			success: function(response){
				if (response['success']){
					console.log(response);
					$('#update_sub_comment').val(response['fields']['text']);
					$('#update_sub_comment_id').val(response['fields']['pk']);
					$('#sub_comment_model').modal('toggle');
				}else{
					display_popup_message('Oops Some error occured..!! :(', 'error');
				}
			},

			error: function(response){
				display_popup_message('Oops Some error occured..! :(', 'error');
			},		
		});
	});

	$('#update_sub_cmd_btn').click(function(){
		var text = $('#update_sub_comment').val();
		var id = $('#update_sub_comment_id').val();
		if (text && id){
			$.ajax({
				type : 'post',
				url : '/blog/post/comment/add_update_subcomment',
				data: {
					sub_comment_id : id,
					action: 'edit',
					text: text,
					csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
				},

				success: function(response){
					if(response['success']){
						$('#sub_comment_text_'+id).html(text);
						 $('#sub_comment_model').modal('toggle');
						display_popup_message('Comment updated successfully..! :)', 'success');

					}
				},

				error: function(){
					display_popup_message('Oops Some error occured..! :(', 'error');
				},
			});
		}
	});
});
