
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from django.views.generic import View, UpdateView, CreateView
from django.contrib import messages
from user_auth.forms import LoginForm, UserForm, PasswordForm, RegisterForm
from user_auth.models import UserDetails
from django.conf import settings
from slugify import slugify


# Create your views here.

class LoginView(View):
	template_name = 'user_auth/login.html'
	context = {}

	def post(self, request):
		form = LoginForm(request.POST)

		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username=username, password=password)
			if user:
				if user.is_active:
					login(request, user)
					messages.add_message(request, messages.SUCCESS, "Welcome "+str(request.user.first_name)+ '!!')
					return HttpResponseRedirect(reverse('blog:home'))
				else:
					self.context['error'] = 'This user is inactive'
			else:
				self.context['error'] = 'Invalid username or password.'	
		else:
			self.context['error'] = 'Please enter username and password.!!'
		return render(request, self.template_name, self.context)

	def get(self, request):
		if request.user.is_authenticated:
			return HttpResponseRedirect(reverse('blog:home'))
		return render(request, 'user_auth/login.html')

class LogoutView(View):
	def get(self, request):
		logout(request)
		messages.add_message(request, messages.SUCCESS, "Successfully logged out. See you again!!")
		return HttpResponseRedirect(reverse('blog:home'))

class UserDetailsView(LoginRequiredMixin, UpdateView):
	model = User
	template_name = 'user_auth/user_details.html'
	form_class = UserForm

	def get_object(self, queryset=None):
		slug = self.kwargs.get('slug', None)
		try:
			return User.objects.get(user_details__slug= slug)
		except User.DoesNotExist:
			return ''

	def form_valid(self, form):
		self.object = form.save()
		try:
			userdetails = UserDetails.objects.get(user=self.object)
		except UserDetails.DoesNotExist:
			userdetails = ''

		if userdetails:
			userdetails.middle_name = form.cleaned_data.get('middle_name')
			userdetails.save()
		messages.add_message(self.request, messages.SUCCESS, "Profile updated successfully..!!")
		return super(UserDetailsView,self).form_valid(form)
	
	def get_success_url(self):
		return reverse('blog:home')

class UpdatePasswordView(LoginRequiredMixin, View):

	def post(self, request):
		
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)
			return JsonResponse({'success': 1, 'message': 'Password updated successfully.. :)'})
		else:
			print(form.errors.as_json())
			return JsonResponse({'error': 1, 'message': 'Oops..! Update failed.. Please fix the errors', 'form_errors': form.errors.as_json()})


class RegisterView(CreateView):
	form_class = RegisterForm
	template_name = 'user_auth/register.html'

	def form_valid(self, form):
		print(form.cleaned_data['first_name'])
		self.obj = form.save(commit=False)
		self.obj.set_password(form.cleaned_data['password'])
		self.obj.save()
		if self.obj.username:
			# add user details data
			UserDetails.objects.create( user=self.obj, 
											middle_name=form.cleaned_data.get('middle_name'), 
											slug=slugify(self.obj.username))

			current_user = authenticate(username=self.obj.username, password=form.cleaned_data['password'])
			login(self.request, current_user)
			messages.add_message(self.request, messages.SUCCESS, 'Welcome '+ current_user.first_name)
		else:
			messages.add_message(self.request, messages.SUCCESS, 'Oops..!! Some error occured..!!')
		return super(RegisterView, self).form_valid(form)

	def get_success_url(self):
		return reverse('blog:home')