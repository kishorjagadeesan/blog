#urls.py

from django.conf.urls import url
from user_auth import views

app_name = 'user'

urlpatterns = [
	url(r'login', views.LoginView.as_view(), name="login"),
	url(r'logout', views.LogoutView.as_view(), name="logout"),
	url(r'user_details/(?P<slug>[-\w]+)', views.UserDetailsView.as_view(), name="user_details"),
	url(r'update_password', views.UpdatePasswordView.as_view(), name="update_password"),
	url(r'register', views.RegisterView.as_view(), name="register"),
]