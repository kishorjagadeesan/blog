#forms.py

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from slugify import slugify
from user_auth.models import UserDetails

class UserForm(forms.ModelForm):
	email = forms.EmailField(required=True)
	first_name = forms.CharField(required=True)
	last_name = forms.CharField(required=True)
	middle_name = forms.CharField(required=False)
	class Meta():
		model = User
		fields = ('email', 'first_name', 'last_name')

class LoginForm(forms.Form):
	username = forms.CharField(required=True)
	password = forms.CharField(required=True)


class PasswordForm(forms.Form):
	password = forms.CharField(required=True)
	new_password = forms.CharField(required=True)
	confirm_password = forms.CharField(required=True)

	def clean(self):
		form_data = super().clean()
		if form_data.get('new_password') != form_data.get('confirm_password'):
			self.add_error('new_password', 'New and confirm password doesnot match..!!');

class RegisterForm(forms.ModelForm):
	first_name = forms.CharField()
	last_name = forms.CharField()
	email = forms.CharField()
	password = forms.CharField(required=True)

	class Meta():
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password',)

	def clean(self):
		clean_data = super(RegisterForm, self).clean()
		password = clean_data.get('password')
		if password:
			try:
				validate_password(password)
			except ValidationError as e:
				print(str(e))
				self.add_error('password', '<br/>'.join(e))

		if clean_data.get('username'):
			try:
				user_details = UserDetails.objects.get(slug=slugify(clean_data.get('username')))
			except:
				user_details= None

			if user_details:
				self.add_error('username', 'username already exist. Please chooes a different username.')
