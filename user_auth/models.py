from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from slugify import slugify


# Create your models here.

class UserDetails(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_details')
	middle_name = models.CharField(max_length=100, blank=True, null=True)
	slug = models.SlugField(max_length=200, blank=True, null=True, unique=True)

	def __str__(self):
		return str(self.user.first_name) + "::" + str(self.slug)

'''
def user_post_save_receiver(sender, instance, created, *args, **kwargs):
	print("inside the signal function:", created)
	if created:
		UserDetails.objects.create(user=instance, middle_name='', slug=slugify(instance.username))



post_save.connect(user_post_save_receiver, sender=User)
'''