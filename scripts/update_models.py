# update_models.py
import os, inspect, sys, getopt, django
from slugify import slugify
# Get the parent Directory and add it to the path to imprt settings.py
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tech_blog.settings')
django.setup()

from django.contrib.auth.models import User
from user_auth.models import UserDetails
from blog.models import Post


def update_model_data(model):
	print(model)
	if not model:
		print('No model given..!!')
		sys.exit(2)

	if model=='User':
		print('Updating users slug field if not exist to slug(username)')
		for user in User.objects.all():
			try:
				user_details = UserDetails.objects.get(user=user)
			except UserDetails.DoesNotExist:
				user_details = ''

			if user_details:
				if not user_details.slug:
					user_details.slug = slugify(user_details.user.username)
			else:
				UserDetails.objects.create(user=user, middle_name='', slug=slugify(user.username))
	elif model=='Post':
		print('Updating Posts slug field if not exist to slug(title)')
		for post in Post.objects.all():
			if not post.slug:
				post.slug = slugify(post.title)
				print('Updating slug field for post: ', post.title )
				post.save()

def parse_arguments(args):
	try:
		options, arg = getopt.getopt(args, 'ht', ['model=', 'help'])
	except getopt.GetoptError as err:
		print (err)
		sys.exit(2)

	for option, val in options:
		if option == '--help' or option == '-h':
			s = """
				==========================================================================
				Note:

				This is not completely developed currently used to perform specific update.
				please review the code before executing.
				==========================================================================
				Script use to update the model fields.
				run with -t for 'test mode' which will not update the database but will output
				the rows which going to update. provide model name using --model argument
				eg: 
					1. test mode: 	update_models.py -t --model=User
					2. update mode: update_models.py --model=User
			"""
			print(s)
		elif option == '--model':
			update_model_data(val)
		else:
			print("Script not yet configured for this argument.")
			sys.exit(2)


if __name__ == "__main__":
	parse_arguments(sys.argv[1:])