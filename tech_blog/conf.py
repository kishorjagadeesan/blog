# conf.py

configuration = {
	
	'dev': {
				'hosts': [
					'localhost',
					'127.0.0.1'
				]
	},

	'qa': {
				'hosts': [
					'localhost',
					'127.0.0.1'
				]
	},

	'prod': {
				'hosts': [
					'kishorjagadeesan.pythonanywhere.com',
				]
	},

	'unknown':
		{
			'hosts': [],
		}
}
