'''
DJANGO PROJECT

Note: This is a learning project so the code is not optimized.
Please read this document for setting up the project.

Sample blog Application. Users can login and add posts and add comment to the blog.

'''

Project Name: tech_blog
python_version: 3.5.2
django_version: 2.0.5
database_used: mysql
database_name: tech_blog
Add the host and other configuration details on conf.py file for each environment.


'''
	Set the folowing environment variables.

'''

	1. TECH_BLOG_SEC_KEY: Secret key for the Django project
	2. DB_USER			: Database username
	3. DB_PASSWORD		: Database password.
	4. DB_HOSTNAME		: SQL server Hostname
	5. DB_PORT			: SQL server port.
	6. CURRENT_ENV		: (dev, qa, prod)
	7. DEBUG_STATUS		: (0 or 1)


'''
	For any details please contact: kishorkrishnalayam@gmail.com.
'''
